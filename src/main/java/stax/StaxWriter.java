/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stax;





import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;



import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


public class StaxWriter {
  private String configFile;
 

  public void setFile(String configFile) {
    this.configFile = configFile;
  }

  public void saveConfig(ArrayList<Game> list) throws Exception {
	 String xmlstr = null;
	 StringWriter writerStr = new StringWriter();
	 
    // create an XMLOutputFactory
    XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
    
    // create XMLStreamWriter
    XMLStreamWriter streamWriter = outputFactory.createXMLStreamWriter(new FileOutputStream (configFile));
      
    // create and write Start Tag
    streamWriter.writeStartDocument();
    
    // starting the element
    streamWriter.writeStartElement("", "", "videogames");
    
    // game
    for (Game game : list){
  	    
  	    //add game element start
  	    streamWriter.writeStartElement("game");
  	    
  	    //write title
  	    streamWriter.writeStartElement("title");
  	    streamWriter.writeCharacters(game.getTitle());
  	    streamWriter.writeEndElement();
  	    
  	    //end game
  	    streamWriter.writeEndElement();
    	
  	}
  	// end videogames
  	streamWriter.writeEndElement();
  	
  	//end document
  	streamWriter.writeEndDocument();
  	
  	//flush
  	streamWriter.flush();
  	
  	//end stream
  	streamWriter.close();
  	
  	xmlstr = writerStr.getBuffer().toString();
  	writerStr.close();
  	
  }

} 
