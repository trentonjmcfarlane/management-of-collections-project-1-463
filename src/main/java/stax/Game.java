/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stax;

/**
 *
 * @author cs463001_25
 */
public class Game {
    private String title;
    private String genre;
    private String developer;
    private String year;
    private int id;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * @return the developer
     */
    public String getDeveloper() {
        return developer;
    }

    /**
     * @param developer the developer to set
     */
    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }
    
    public Game(){
        
    }
    
    public Game(String title, String genre, String developer, String year, int id){
    	this.title = title;
    	this.genre = genre;
    	this.developer = developer;
    	this.year = year;
    	this.setId(id);
    }
    
    @Override
    public String toString(){
		String toString = "";
		toString = this.title + " " + genre + " " 
				+ developer + " " + year;
		return toString;
    	
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
}
