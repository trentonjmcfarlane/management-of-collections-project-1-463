/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;



  public class StaxParser {
  static final String GAME = "game";
  static final String TITLE = "title";
  static final String GENRE = "genre";
  static final String DEVELOPER = "developer";
  static final String YEAR = "year";

  @SuppressWarnings({ "unchecked" })
  public ArrayList<Game> readConfig(String configFile) {
    ArrayList<Game> items = new ArrayList<Game>();
    try {
      // First, create a new XMLInputFactory
      XMLInputFactory inputFactory = XMLInputFactory.newInstance();
      // Setup a new eventReader
      InputStream in = new FileInputStream(configFile);
      XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
      // read the XML document
      Game item = null;

      while (eventReader.hasNext()) {
        XMLEvent event = eventReader.nextEvent();

        if (event.isStartElement()) {
          StartElement startElement = event.asStartElement();
          // If we have an item element, we create a new item
          if (startElement.getName().getLocalPart().equals(GAME)) {
            item = new Game();
            // We read the attributes from this tag and add the date
            // attribute to our object
            Iterator<Attribute> attributes = startElement
                .getAttributes();
            while (attributes.hasNext()) {
              Attribute attribute = attributes.next();
              if (attribute.getName().toString().equals(TITLE)) {
                item.setTitle(attribute.getValue());
              }
            }
          }
          if (event.isStartElement()) {
              if (event.asStartElement().getName().getLocalPart()
                  .equals(TITLE)) {
                event = eventReader.nextEvent();
                item.setTitle(event.asCharacters().getData());
                continue;
              }
            }
          if (event.isStartElement()) {
            if (event.asStartElement().getName().getLocalPart()
                .equals(GENRE)) {
              event = eventReader.nextEvent();
              item.setGenre(event.asCharacters().getData());
              continue;
            }
          }
          if (event.asStartElement().getName().getLocalPart()
              .equals(DEVELOPER)) {
            event = eventReader.nextEvent();
            item.setDeveloper(event.asCharacters().getData());
            continue;
          }

          if (event.asStartElement().getName().getLocalPart()
              .equals(YEAR)) {
            event = eventReader.nextEvent();
            item.setYear(event.asCharacters().getData());
            continue;
          }

        }
        // If we reach the end of an item element, we add it to the list
        if (event.isEndElement()) {
          EndElement endElement = event.asEndElement();
          if (endElement.getName().getLocalPart() == (GAME)) {
            items.add(item);
          }
        }

      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (XMLStreamException e) {
      e.printStackTrace();
    }
    return items;
  }

} 
