package videoGameDMBS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import stax.*;

public class XMLController extends VideoGameDMBS{
	public static String filename;
	
	public static ArrayList<Game> Read(String path){
		filename = path;
	    StaxParser read = new stax.StaxParser();
	    ArrayList<Game> readConfig = read.readConfig(path);
	    return readConfig;
	}
	
	public static String getFileName(){
		return XMLController.filename;
	}
	
	public static void Write(){
	    StaxWriter configFile = new StaxWriter();
	    configFile.setFile("src/main/resources/videogames2.xml");
	    try {
	      configFile.saveConfig();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public void populateFirstList(){
		
	}

}
