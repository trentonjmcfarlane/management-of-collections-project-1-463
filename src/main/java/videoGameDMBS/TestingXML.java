/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package videoGameDMBS;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
 
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import stax.Game;
 
public class TestingXML {
 
    public static void main(String[] args) {
        String fileName = "src/main/resources/test.xml";
        String rootElement = "game";
        TestingXML xmlStreamWriter = new TestingXML();
        
        ArrayList<Game> gameList = new ArrayList<Game>();
        Game game = new Game("Halo", "Action", "Bingie", "2001", 1);
        Game game1 = new Game("Halo 2", "Action", "Bingie", "2008", 2);
        gameList.add(game);
        gameList.add(game1);
        
        
	    xmlStreamWriter.writeXML(fileName, rootElement, gameList);
    }
 
    private void writeXML(String fileName, String rootElement, ArrayList<Game> elementList) {
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        try{
            XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileOutputStream(fileName), "UTF-8");
            //start writing xml file
            xmlStreamWriter.writeStartDocument("UTF-8", "1.0");
            xmlStreamWriter.writeCharacters("\n");
            xmlStreamWriter.writeStartElement("videogames");
            
            Iterator<Game> itr = elementList.iterator();
          	while (itr.hasNext()){
          		Game newGame = new Game();
          	    newGame = itr.next();
          	    
            xmlStreamWriter.writeStartElement(rootElement);
             
            //write title as attribute
            xmlStreamWriter.writeAttribute("id", String.valueOf(newGame.getId()));
             
            //write other elements
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeStartElement("title");
            xmlStreamWriter.writeCharacters("\n\t\t"+newGame.getTitle());
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeEndElement();
            
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeStartElement("genre");
            xmlStreamWriter.writeCharacters("\n\t\t"+newGame.getGenre());
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeEndElement();
             
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeStartElement("developer");
            xmlStreamWriter.writeCharacters("\n\t\t"+newGame.getDeveloper());
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeEndElement();
             
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeStartElement("year");
            xmlStreamWriter.writeCharacters("\n\t\t"+newGame.getYear());
            xmlStreamWriter.writeCharacters("\n\t");
            xmlStreamWriter.writeEndElement();
            //write end tag of Employee element
            xmlStreamWriter.writeCharacters("\n");
            xmlStreamWriter.writeEndElement();
          	}
          	
            xmlStreamWriter.writeEndElement();
             
            //write end document
            xmlStreamWriter.writeEndDocument();
             
            //flush data to file and close writer
            xmlStreamWriter.flush();
            xmlStreamWriter.close();
             
        }catch(XMLStreamException | FileNotFoundException e){
            e.printStackTrace();
        }
    }
 
}