package videoGameDMBS;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import stax.*;

import org.eclipse.swt.widgets.List;
import org.junit.Test;

public class VideoGameDMBSTest {

	@Test
	public void testupdateDataList() {
		ArrayList<Game> list1 = new ArrayList<Game>();
		ArrayList<Game> list2 = new ArrayList<Game>();
		Game game1 = new Game();
		Game game2 = new Game();
		
		game1.setDeveloper("Bungie");
		game1.setGenre("Action");
		game1.setYear("2001");
		game1.setTitle("Halo");
		
		game2.setDeveloper("Bingie");
		game2.setGenre("Action");
		game2.setYear("2001");
		game2.setTitle("Halo");
		
		list1.add(game1);
		list2.add(game2);
		
		ArrayList<String> newList1 = new ArrayList<String>();
		ArrayList<String> newList2 = new ArrayList<String>();
		
		newList1 = VideoGameDMBS.updateDataList(list1, "dev");
		newList2 = VideoGameDMBS.updateDataList(list2, "dev");
		
		System.out.println(newList1);
		System.out.println(newList2);
	}
	
	@Test
	public void testLoad(){
		String filePath = "src/main/resources/videogames.xml";
		VideoGameDMBS.load(filePath);
		ArrayList<Game> temp = VideoGameDMBS.getMainList();
		Iterator<Game> itr = temp.iterator();
		while (itr.hasNext()){
			System.out.println("MainList: " + itr.next());
		}
		System.out.println("CurrentDataList: " + VideoGameDMBS.getCurrentData());
	}
}
