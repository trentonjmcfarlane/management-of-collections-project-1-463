package videoGameDMBS;

import java.awt.LayoutManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.List;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Button;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Group;

import stax.*;
import swing2swt.layout.BorderLayout;


public class VideoGameDMBS extends ApplicationWindow {

	protected static ArrayList<Game> mainList = new ArrayList<Game>();
	private static ArrayList<String> currentData = new ArrayList<String>();
	private List list;
	private List list2;
	private List list_1;
	private List list_2;
	private List list_3;
	private List list_4;
	private Label lblTest;
	private Label lblTest_1;
	private static Label lblCurrentDatabase;
	private static Button btnAddGame;
	
	public static void displayFileName(String fileName){
		lblCurrentDatabase.setText(fileName);
	}
	
	
	public static ArrayList<String> updateDataList(ArrayList<Game> gameList, String searchParam){
		ArrayList<String> newList = new ArrayList<String>();
		Iterator<Game> itr = gameList.iterator();
		while (itr.hasNext()){
			Game game = (Game)itr.next();
			switch (searchParam) {
            case "dev":	newList.add(game.getDeveloper());
                     break;
            case "title":  newList.add(game.getTitle());
                     break;
            case "genre":  newList.add(game.getGenre());
                     break;
            case "year":  newList.add(game.getYear());
                     break;
            default: newList.add(game.getDeveloper());
                     break;
			}
			//currentData = newList;
		}
		return newList;
	}
	
	public void displayData(){
		if (currentData.size() == 0){
			list.removeAll();
		}
		else {
			list.removeAll();
			Iterator<String> itr = currentData.iterator();
			while(itr.hasNext()){
				String temp = itr.next();
				list.add((String)temp);

			}
		}
	}
	
	public static void addGame(Game newGame){
		mainList.add(newGame);
		currentData = updateDataList(mainList, "genre");
	}
	
	public static ArrayList<String> getCurrentData(){
		return currentData;
	}
	
	public static ArrayList<Game> getMainList(){
		return mainList;
	}
	
	public static void load(String filePath){
		mainList = XMLController.Read(filePath);
		currentData = updateDataList(mainList, "genre");
	}
	
	public void saveDatabase(){
		
		StaxWriter configFile = new StaxWriter();
	    configFile.setFile("src/main/resources/videogames3.xml");
	    try {
	      configFile.saveConfig(mainList);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
		
	}
	
	
	public static void addGameErrorPopup(){
		String infoMessage = "Must choose database first!";
		JOptionPane.showMessageDialog(null, infoMessage, infoMessage, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void addGamePopup(){
		JTextField field1 = new JTextField(20);
        JTextField field2 = new JTextField(20);
        JTextField field3 = new JTextField(20);
        JTextField field4 = new JTextField(20);
		JPanel panel = new JPanel();
		
	
        panel.add(new JLabel("Title:"));
        panel.add(field1);
        panel.add(new JLabel("Genre:"));
        panel.add(field2);
        panel.add(new JLabel("Developer:"));
        panel.add(field3);
        panel.add(new JLabel("Year Developed:"));
        panel.add(field4);
       
        
        int result = JOptionPane.showConfirmDialog(null, panel, "Add a game menu",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            Game newGame = new Game();
            newGame.setTitle(field1.getText());
            newGame.setGenre(field2.getText());
            newGame.setDeveloper(field3.getText());
            newGame.setYear(field4.getText());
            addGame(newGame);
            list.removeAll();
            displayData();
        } else {
            
        }
		
	}
	/**
	 * Create the application window.
	 */
	public VideoGameDMBS() {
		super(null);
		addMenuBar();
		addStatusLine();
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(6, false));
		{
			Group group = new Group(container, SWT.NONE);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 4));
			group.setLayout(new GridLayout(1, false));
			{
				Button btnOpen = new Button(group, SWT.NONE);
				btnOpen.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				btnOpen.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						JFileChooser chooser = new JFileChooser();
						FileNameExtensionFilter filter = new FileNameExtensionFilter(
						        "XML Files", "xml");
						    chooser.setFileFilter(filter);
						    File dir = new File("src/main/resources");
						    chooser.setCurrentDirectory(dir);
					    int returnVal = chooser.showOpenDialog(null);
					    if(returnVal == JFileChooser.APPROVE_OPTION) {
					       load(chooser.getSelectedFile().getAbsolutePath());
					       displayFileName(chooser.getSelectedFile().getName());
					    }
					}
				});
				btnOpen.setText("Open");
			}
			{
				Button btnSave = new Button(group, SWT.NONE);
				btnSave.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						saveDatabase();
					}
				});
				btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				btnSave.setText("Save");
			}
			{
				Button btnClose = new Button(group, SWT.NONE);
				btnClose.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						currentData.clear();
						list.removeAll();
						lblCurrentDatabase.setText("No data...");
					}
				});
				btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				btnClose.setText("Close");
			}
			{
				Button btnExit = new Button(group, SWT.NONE);
				btnExit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				btnExit.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						System.exit(0);
					}
				});
				btnExit.setText("Exit");
			}
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			Group group = new Group(container, SWT.NONE);
			group.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1));
			group.setLayout(new GridLayout(1, false));
			{
				lblCurrentDatabase = new Label(group, SWT.NONE);
				GridData gd_lblCurrentDatabase = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
				gd_lblCurrentDatabase.widthHint = 279;
				lblCurrentDatabase.setLayoutData(gd_lblCurrentDatabase);
				lblCurrentDatabase.setText("No data...");
			}
		}
		{
			Button btnAddSelectedDatabase = new Button(container, SWT.NONE);
			btnAddSelectedDatabase.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					displayData();
				}
			});
			btnAddSelectedDatabase.setText("Display Selected Database");
		}
		{
			btnAddGame = new Button(container, SWT.NONE);
			btnAddGame.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (currentData.size() == 0){
						addGameErrorPopup();
					}
					else {
						addGamePopup();
					}
				}
			});
			btnAddGame.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			btnAddGame.setText("Add Game");
		}
		new Label(container, SWT.NONE);
		{
			lblTest = new Label(container, SWT.NONE);
			lblTest.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblTest.setText("Genre");
		}
		{
			lblTest_1 = new Label(container, SWT.NONE);
			lblTest_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblTest_1.setText("Developer");
		}
		{
			Label lblTest_2 = new Label(container, SWT.NONE);
			lblTest_2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblTest_2.setText("Year");
		}
		{
			Label lblTest_3 = new Label(container, SWT.NONE);
			lblTest_3.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblTest_3.setText("Game");
		}
		{
			Label lblTest_4 = new Label(container, SWT.NONE);
			lblTest_4.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			lblTest_4.setText("Info");
		}
		{
			ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gd_scrolledComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
			gd_scrolledComposite.widthHint = 126;
			gd_scrolledComposite.heightHint = 347;
			scrolledComposite.setLayoutData(gd_scrolledComposite);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.setExpandVertical(true);
			{
				list = new List(scrolledComposite, SWT.BORDER);
				
				list.addSelectionListener( new SelectionListener() {

					public void widgetDefaultSelected(SelectionEvent e) {
						
						
					}

					public void widgetSelected(SelectionEvent e) {
						
					}
					
				});
				
			}
			scrolledComposite.setContent(list);
			scrolledComposite.setMinSize(list.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
		{
			ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gd_scrolledComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
			gd_scrolledComposite.widthHint = 126;
			gd_scrolledComposite.heightHint = 347;
			scrolledComposite.setLayoutData(gd_scrolledComposite);
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			{
				list_1 = new List(scrolledComposite, SWT.BORDER);
			}
			scrolledComposite.setContent(list_1);
			scrolledComposite.setMinSize(list_1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
		{
			ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gd_scrolledComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
			gd_scrolledComposite.widthHint = 126;
			gd_scrolledComposite.heightHint = 347;
			scrolledComposite.setLayoutData(gd_scrolledComposite);
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			{
				list_2 = new List(scrolledComposite, SWT.BORDER);
			}
			scrolledComposite.setContent(list_2);
			scrolledComposite.setMinSize(list_2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
		{
			ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gd_scrolledComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
			gd_scrolledComposite.widthHint = 126;
			gd_scrolledComposite.heightHint = 347;
			scrolledComposite.setLayoutData(gd_scrolledComposite);
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			{
				list_3 = new List(scrolledComposite, SWT.BORDER);
			}
			scrolledComposite.setContent(list_3);
			scrolledComposite.setMinSize(list_3.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
		{
			ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gd_scrolledComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
			gd_scrolledComposite.widthHint = 126;
			gd_scrolledComposite.heightHint = 347;
			scrolledComposite.setLayoutData(gd_scrolledComposite);
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			{
				list_4 = new List(scrolledComposite, SWT.BORDER);
			}
			scrolledComposite.setContent(list_4);
			scrolledComposite.setMinSize(list_4.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}

		return container;
	}



	/**
	 * Create the status line manager.
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			VideoGameDMBS window = new VideoGameDMBS();
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Data Management System");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(1200, 800);
	}
}
